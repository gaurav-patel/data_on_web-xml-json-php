$(function(){
	
	//Activating navigation menu item -- highlight current nav
	$('#home a:contains("Home")').parent().addClass('active');
	$('#csv a:contains("CSV")').parent().addClass('active');
	$('#xml a:contains("XML")').parent().addClass('active');
	$('#json a:contains("JSON")').parent().addClass('active');
	
});
