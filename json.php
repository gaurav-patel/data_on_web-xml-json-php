<?php
	if($_POST){
		$post = $_POST;
		//removing submit field from post because we don't want to save that in xml file
		unset($post['submit']);
		//adding date field to post because we want to add time stamp of when user checked in
		$post['Date'] = date("Y-m-d H:i:s");
		
		//reading file..
		$inp = file_get_contents('json_data.json');
		$tempArray = json_decode($inp, true);
		//inserting form data into guests array
		array_push($tempArray['guests'], $post);
		//reverting data back to json format
		$jsonData = json_encode($tempArray, JSON_PRETTY_PRINT);
		//saving data to file
		file_put_contents('json_data.json', $jsonData);

	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Data on Web - JSON Demo</title>
		<meta name="viewport" content="width=device-width, initial-scale:1.0" />
		<link href="../Bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="script.js"></script>
		
		<script type="text/javascript">
			//using ajax to load json file and call loadJSON function on complete passing in json file
			$(document).ready(function(){
				$.ajax({
			        type: 'GET',
			        url: 'json_data.json',
			        dataType: 'json',
			        success: loadJSON
			    });
			});
			//Adding data from json file to page
			function loadJSON(json){
				$.getJSON('json_data.json', function(data){
					$.each(data.guests, function(key, value){
						$("#jsonData").append('<tr><td>' + value.Name + '</td><td>' + value.Profession + '</td>'
							+ '<td>' + value.Age + '</td><td>' + value.Date + '</td></tr>');
					});
				});
			}
		</script>
	</head>
	<body id="json">
		
		<section class="container">
			<div class="content row">
				<?php include 'components/header.php'; ?>
					
				<section class="main col col-lg-12">
					<div class="panel panel-primary">
						<div class="panel-heading">Check In</div>
						<div class="panel-body">
							
							<form class="form-inline" method="post">
								<div class="form-group">
							    	<label for="Name">Full Name</label>
							    	<input type="text" class="form-control" name="Name" id="Name" />
							  	</div>
							  	<div class="form-group">
							    	<label for="profession">Profession</label>
							    	<input type="text" class="form-control" name="Profession" id="profession" />
							  	</div>
							  	<div class="form-group">
							    	<label for="age">Age</label>
							    	<input type="number" min="10" max="70" class="form-control" name="Age" id="age" />
							  	</div>
							  	<input type="submit" name="submit" id="submit" class="btn btn-default" value="Submit" />
							</form> <!-- form-inline -->
							
						</div> <!-- panel-body -->
					</div> <!-- panel panel-primary -->
					
					<div class="panel panel-success">
						<div class="panel-heading">Checked In Users</div>
					  	<div class="panel-body">
					    	<p>The data submit by the form is saved in json file and displayed using JavaScript, Check out 
					    		<a href="https://bitbucket.org/gaurav-patel/data_on_web">Bitbucket Repository</a> to find out 
					    		how to read from and write to json file and click on <a href="json_data.json">here</a> to see JSON file.</p>
					  	</div>
					
						<table class="table" id="jsonData">
					    	<tr>
					    		<th>Name</th>
					    		<th>Profession</th>
					    		<th>Age</th>
					    		<th>Date</th>
					    	</tr>

					 </table> <!-- table jsonData -->
					</div>
				</section> <!-- main col col-lg-12  -->
				
				<?php include 'components/footer.php'; ?>
			</div> <!-- content row  -->
		</section> <!-- container  -->
		
	</body>
</html>