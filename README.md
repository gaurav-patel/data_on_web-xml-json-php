# README #

This repository contains of demo projects regarding how to save data from forms to CSV, XML, and JSON files using PHP, JavaScript (little but of jQuery and Ajax) and Bootstrap for responsive design.

![Screenshot of Demo Site](/screenshot.jpg "Screenshot of Demo Site")