<?php
	
	if($_POST) {
		$post = $_POST;
		//removing submit field from post because we don't want to save that in csv file
		unset($post['submit']);
		//adding date field to post because we want to add time stamp of when user checked in
		$post['Date'] = date("Y-m-d H:i:s");

		// keys are headers 
    	$header = array_keys($post);
		// get values
		$data = array_values($post);

		// open file to read and write data
		if($fp = fopen('csv_data.csv','a+')){
			// get first line, heading
			$line = fgets($fp);
			
			//check and see if first line is same as heading line, if not then write the heading line
			if(!$line == $header){		
				fputcsv($fp, $header);
				fputcsv($fp, $data);
			} else { //just append the data
				fputcsv($fp, $data); 
			}
			fclose($fp);
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Data on Web - CSV Demo</title>
		<meta name="viewport" content="width=device-width, initial-scale:1.0" />
		<link href="../Bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="script.js"></script>

	</head>
	<body id="csv">
		
		<section class="container">
			<div class="content row">
				<?php include 'components/header.php'; ?>
					
				<section class="main col col-lg-12">
					<div class="panel panel-primary">
						<div class="panel-heading">Check In</div>
						<div class="panel-body">
							
							<form class="form-inline" method="post">
								<div class="form-group">
							    	<label for="Name">Full Name</label>
							    	<input type="text" class="form-control" name="Name" id="Name" placeholder="First Last" />
							  	</div>
							  	<div class="form-group">
							    	<label for="profession">Profession</label>
							    	<input type="text" class="form-control" name="Profession" id="profession" placeholder="Web Developer"/>
							  	</div>
							  	<input type="submit" name="submit" id="submit" class="btn btn-default" value="Submit" />
							</form> <!-- form-inline -->
							
						</div> <!-- panel-body -->
					</div> <!-- panel panel-primary -->
					
					<div class="panel panel-success">
						<div class="panel-heading">Checked In Users</div>
					  	<div class="panel-body">
					    	<p>Check out <a href="https://bitbucket.org/gaurav-patel/data_on_web">Bitbucket Repository</a> to find out how 
					    		to read from and write to csv file and click on <a href="csv_data.csv">here</a> to see CSV file.</p>
					  	</div>
					
						<?php
					    	//open the file to read data
					    	if($fp = fopen('csv_data.csv', 'r')){
					    ?>
						<table class="table">
					    	<tr>
					    		<?php 
					    			$data = fgetcsv($fp);
					    			echo "<th>" . $data[0]. "</th>";
									echo "<th>" . $data[1]. "</th>";
									echo "<th>" . $data[2]. "</th>";	
					    		?> 
					    	</tr>
					    	<?php 
					    		while(!feof($fp)){
					    			$data = fgetcsv($fp);
									echo "<tr><td>" . $data[0] . "</td>";
									echo "<td>" . $data[1] . "</td>";
									echo "<td>" . $data[2] . "</td></tr>";	
								} 
							?>					    	
					 </table> <!-- table -->
					 <?php 
						}
						fclose($fp);
					 ?>
					</div>
				</section> <!-- main col col-lg-12  -->
				
				<?php include 'components/footer.php'; ?>
			</div> <!-- content row  -->
		</section> <!-- container  -->
		
	</body>
</html>