<?php
    if($_POST){
    	$post = $_POST;
		//removing submit field from post because we don't want to save that in xml file
		unset($post['submit']);
		//adding date field to post because we want to add time stamp of when user checked in
		$post['Date'] = date("Y-m-d H:i:s");
		
		//file to save data to.
		$file = 'xml_data.xml';
		
		//creating new DOMDocument element and loading our file into that object.
		$doc = new DOMDocument('1.0');
		$doc->load($file);
		$doc->preserveWhiteSpace = false;   
	    $doc->formatOutput = true;

		
		//getting root element and adding 'user' element to the DOM object.
		$root = $doc->documentElement;
		$user = $doc->createElement('user');
    	$user = $root->appendChild($user);
		
		//looping through $post elements and adding them to DOM object's user element
		foreach ($post as $key => $value) {
	        $node = $doc->createElement($key, $value);
			$user->appendChild($node);
	    }
		
		//saving DOM object to my file.
		$doc->save($file) or die("Sorry, there's a problem saving the file.");
    }
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Data on Web - XML Demo</title>
		<meta name="viewport" content="width=device-width, initial-scale:1.0" />
		<link href="../Bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" />
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="script.js"></script>
		
		<script type="text/javascript">
			//using ajax to load xml file and call loadXML function on complete passing in xml file
			$(document).ready(function(){
				$.ajax({
			        type: "GET",
			        url: "xml_data.xml",
			        dataType: "xml",
			        success: loadXML
			    });
			});
			
			//Adding data from xml file to page
			function loadXML(xml){
				$(xml).find("user").each(function(){
					$("#xmlData").append('<tr class="data_from_xml"><td>' + $(this).find("Name").text() + '</td><td>' 
						+ $(this).find("Profession").text() + '</td><td>' + $(this).find("Age").text() + '</td><td>' 
						+ $(this).find("Date").text() + '</td></tr>');
				});	
			}
		</script>
	</head>
	<body id="xml">
		
		<section class="container">
			<div class="content row">
				<?php include 'components/header.php'; ?>
					
				<section class="main col col-lg-12">
					<div class="panel panel-primary">
						<div class="panel-heading">Check In</div>
						<div class="panel-body">
							
							<form class="form-inline" method="post">
								<div class="form-group">
							    	<label for="Name">Full Name</label>
							    	<input type="text" class="form-control" name="Name" id="Name" />
							  	</div>
							  	<div class="form-group">
							    	<label for="profession">Profession</label>
							    	<input type="text" class="form-control" name="Profession" id="profession" />
							  	</div>
							  	<div class="form-group">
							    	<label for="age">Age</label>
							    	<input type="number" min="10" max="70" class="form-control" name="Age" id="age" />
							  	</div>
							  	<input type="submit" name="submit" id="submit" class="btn btn-default" value="Submit" />
							</form> <!-- form-inline -->
							
						</div> <!-- panel-body -->
					</div> <!-- panel panel-primary -->
					
					<div class="panel panel-success">
						<div class="panel-heading">Checked In Users</div>
					  	<div class="panel-body">
					    	<p>The data submit by the form is saved in XML file and displayed using JavaScript, Check out 
					    		<a href="https://bitbucket.org/gaurav-patel/data_on_web">Bitbucket Repository</a> to find out 
					    		how to read from and write to xml file and click on <a href="xml_data.xml">here</a> to see XML file.</p>
					  	</div>
					
						<table class="table" id="xmlData">
					    	<tr>
					    		<th>Name</th>
					    		<th>Profession</th>
					    		<th>Age</th>
					    		<th>Date</th>
					    	</tr>

					 </table> <!-- table xmlData -->
					</div>
				</section> <!-- main col col-lg-12  -->
				
				<?php include 'components/footer.php'; ?>
			</div> <!-- content row  -->
		</section> <!-- container  -->
		
	</body>
</html>