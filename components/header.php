<nav class="navbar navbar-default">
	<div class="navbar-header">
    	<a class="navbar-brand" href="index.php">Data on Web</a>
	</div> <!-- navbar-header -->
	<ul class="nav navbar-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="csv.php">CSV</a></li>
		<li><a href="xml.php">XML</a></li>
		<li><a href="json.php">JSON</a></li>
	</ul> <!-- nav navbar-nav -->
</nav> <!-- navbar -->